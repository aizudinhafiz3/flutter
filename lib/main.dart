import 'package:flutter/material.dart';
import 'package:myapplication/column.dart';
import 'package:myapplication/expandcolumn.dart';
import 'package:myapplication/flow.dart';
import 'package:myapplication/gridview.dart';
import 'package:myapplication/listview.dart';
import 'package:myapplication/row.dart';
import 'package:myapplication/stack.dart';
import 'package:myapplication/table.dart';

void main() =>
  runApp(MyApp());


class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(title: 'Column'),
    );
  }
}

class MyHomePage extends StatelessWidget {
  MyHomePage({Key? key, required this.title}) : super(key: key);

  final String title;

@override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Flutter'),
      ),
      body: Center(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children:<Widget> [
            ElevatedButton(
          child: Text('Stack'),
          onPressed: () {
            Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => Stacks()),
            );
          },
        ),
        ElevatedButton(
          child: Text('Column'),
          onPressed: () {
            Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => Columns(title: 'colums',)),
            );
          },
        ),
        ElevatedButton(
          child: Text('ExpandColums'),
          onPressed: () {
            Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => ExpandColumns(title: 'Expand Column',)),
            );
          },
        ),
        ElevatedButton(
          child: Text('Flow'),
          onPressed: () {
            Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => Flows(title: 'Flow',)),
            );
          },
        ),
        ElevatedButton(
          child: Text('Grid View'),
          onPressed: () {
            Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => GridViews(title: 'gridView',)),
            );
          },
        ),
        ElevatedButton(
          child: Text('ListView'),
          onPressed: () {
            Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => ListViews(title: 'ListView',)),
            );
          },
        ),
        ElevatedButton(
          child: Text('Row'),
          onPressed: () {
            Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => Rows(title: 'Row',)),
            );
          },
        ),
        ElevatedButton(
          child: Text('Table'),
          onPressed: () {
            Navigator.push( 
              context,
              MaterialPageRoute(builder: (context) => Tables(title: 'Table',)),
            );
          },
        ),
          ],
        )

      ),
      
    );
  }
 }



