import 'package:flutter/material.dart';

class ListViews extends StatelessWidget {
  const ListViews({Key? key,required this.title}) : super(key: key);

    final String title;

@override
 Widget build(BuildContext context) {
 return Scaffold(
   appBar: AppBar(title: Text(this.title)),
 body: Center(
   child:
        ListView(
  padding: const EdgeInsets.all(8),
  children: <Widget>[
    Container(
      height: 50,
      color: Colors.amber[600],
      child: const Center(child: Text('Udin')),
    ),
    Container(
      height: 50,
      color: Colors.amber[500],
      child: const Center(child: Text('Humaira')),
    ),
    Container(
      height: 50,
      color: Colors.amber[100],
      child: const Center(child: Text('Syafiq')),
    ),
    Container(
      height: 50,
      color: Colors.amber[100],
      child: const Center(child: Text('Syafiqah')),
    ),
  ],
        )
 )
 );
 
 }
}